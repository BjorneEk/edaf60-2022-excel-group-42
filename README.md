# EDAF60 - group 42 XL

+ Gustaf Franzén
+ Scott Tollebäck
+ Eliot Vesterlund
+ Viktor Beerman


## Answers to design questions

![Model](images/model.png)

+ **A3**: Explain in just a few words what each of the classes do
```SlotLabel```: en jlabel för en cell \
```SlotLabels```: jlabel för alla celles\
```Editor```:     fältet där man kan modifiera en cell\
```StatusLabel```: visa aplikationens status, ex. eventuella felmedelanden\
```CurrentLabel```: den jlabel som just nu är markerad\
```XL```: main klassen som sätter ihop alla delar och skapar XL appen\


+ **A4**: Use case: Someone writes the number `42` in `Editor`, what
should happen before the value is shown in the view (i.e.,
what path should the value take through M, V, and C)?

V->C->M->C->V


+ **B2**: A cell can contain either a comment, or an expression -- how
do we model that in Java code?

  Klassen ```Cell``` har två subklasser ```ExprCell``` och ```CommentCell```

+ **B4**: What classes, except for those in the `expr` package, do we
neeed in the model?

 ```Cell```,  ```ExprCell```,  ```CommentCell```,  ```CellMatrix```

+ **C1**: When an expression containing an address is calculated, we
use an `Environment` -- why?

  för att kunna få värdet på referenser till ev. andra celler samt köra programmet innan det är färdigt

+ **D1**: What classes in the model must be known to our GUI?

```CellMatrix```

+ **D2**: When our GUI fetches the 'value' of a cell, to display it in
the grid, what value do we want, and what type do we want it
to have?

En sträng, antingen med ett expression eller med en kommentar

+ **E4**: In general: in what package should we detect errors?

  i expr

+ **E5**: In general: in what package should we handle the errors, and
how do we do that?

  i GUI genom att informera andvändaren

+ **F1**: What kind of synchronization (_Flow Synchronization_, or
_Observer Synchronization_) do you want to use in the
communication between M and V/C?

  Flow synchronization

+ **F2**: How does your program keep track of which is the current
cell? The answer to this question depends on how we
implement our Controller.

  ```Controller``` har en ```CurrentLabel``` instans som uppdateras vid input för att referera till rätt label

+ **F3**: How are updates in our GUI triggered? The answer to this
depends on how we implement our Controller.

  ```Controller``` sätter listeners på alla ```SlotLabel```s och uppdaterar modellen beroende på vilken av dessa som aktiveras, sedan uppdateras guit baserat på modellen.


## Running the program

To run the program one can issue:

~~~{.sh}
./gradlew run
~~~

in the root directory of the project.
