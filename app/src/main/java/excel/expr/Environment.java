package excel.expr;

public interface Environment {

  public double value(String name);
}
