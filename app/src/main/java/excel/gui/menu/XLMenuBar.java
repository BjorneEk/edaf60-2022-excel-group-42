package excel.gui.menu;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import excel.gui.StatusLabel;
import excel.gui.XL;
import excel.gui.XLList;

public class XLMenuBar extends JMenuBar {

  public XLMenuBar(XL xl, XLList xlList, StatusLabel statusLabel, Controller controller) {
    JMenu file = new JMenu("File");
    JMenu edit = new JMenu("Edit");
    file.add(new SaveMenuItem(xl, statusLabel, controller));
    file.add(new LoadMenuItem(xl, statusLabel, controller));
    file.add(new NewMenuItem(xl));
    file.add(new CloseMenuItem(xl, xlList));
    edit.add(new ClearMenuItem(controller));
    edit.add(new ClearAllMenuItem(controller));
    add(file);
    add(edit);
    add(new WindowMenu(xlList));
  }
}
