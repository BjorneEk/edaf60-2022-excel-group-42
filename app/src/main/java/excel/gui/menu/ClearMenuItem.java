package excel.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

class ClearMenuItem extends JMenuItem implements ActionListener {

  Controller controller;

  public ClearMenuItem(Controller controller) {
    super("Clear");
    addActionListener(this);
    this.controller = controller;
  }

  public void actionPerformed(ActionEvent e) {
    controller.clearMarked();
  }
}
