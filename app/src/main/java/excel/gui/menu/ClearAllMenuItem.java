package excel.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

class ClearAllMenuItem extends JMenuItem implements ActionListener {

  Controller controller;

  public ClearAllMenuItem(Controller controller) {
    super("Clear all");
    addActionListener(this);
    this.controller = controller;
  }

  public void actionPerformed(ActionEvent e) {
    controller.clearAll();
  }
}
