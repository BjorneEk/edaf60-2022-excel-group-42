package excel.gui.menu;

import java.util.Map;
import java.util.HashMap;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import excel.gui.StatusLabel;
import excel.gui.XL;

class LoadMenuItem extends OpenMenuItem {

  private Controller controller;

  public LoadMenuItem(XL xl, StatusLabel statusLabel, Controller c) {
    super(xl, statusLabel, "Load");
    this.controller = c;
  }

  protected void action(String path) throws FileNotFoundException {
    XLBufferedReader xlBufferedReader = new XLBufferedReader(path);
    Map <String, String> loadMap = new HashMap<>();
    xlBufferedReader.load(loadMap);
    controller.loadNewXL(loadMap);
  }

  protected int openDialog(JFileChooser fileChooser) {
    return fileChooser.showOpenDialog(xl);
  }
}
