package excel.gui.menu;

import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import excel.gui.StatusLabel;
import excel.gui.XL;
import java.util.Set;
import java.util.Map.Entry;

class SaveMenuItem extends OpenMenuItem {

  Controller controller;

  public SaveMenuItem(XL xl, StatusLabel statusLabel, Controller c) {
    super(xl, statusLabel, "Save");
    controller = c;
  }

  protected void action(String path) throws FileNotFoundException {
     controller.saveToFile(path);
  }

  protected int openDialog(JFileChooser fileChooser) {
    return fileChooser.showSaveDialog(xl);
  }
}
