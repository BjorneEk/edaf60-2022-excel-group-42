package excel.gui.menu;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.FileNotFoundException;
import excel.model.CellMatrix;
import excel.model.Cell;
import excel.util.XLException;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;
import excel.gui.*;


public class Controller {

  private CellMatrix      cells;
  private CurrentLabel    current;
  private List<SlotLabel> labels;
  private StatusLabel     status;
  private SlotLabel       marked;
  private Editor          editor;

  public Controller(CellMatrix cells, StatusPanel status, Editor editor, List<SlotLabel> labels) {
    this.cells   = cells;
    this.labels  = labels;
    this.editor  = editor;
    this.status  = status.getStatusLabel();
    this.current = status.getCurrentLabel();

    for (SlotLabel s: labels) {
      s.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
          updateCurrent(s);
        }
      });
    }
    editor.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String s = editor.getText();
        if(s.contains(current.getText())) {
          status.getStatusLabel().setText("the tile: " + current.getText() + "cannot reference to itself");
        } else {
          updateCells(s);
        }
      }
    });
    updateCurrent(labels.get(0));
  }


  private void updateCurrent(SlotLabel label) {
    if (marked != null) {
      marked.setBackground(Color.WHITE);
    }
    if (marked != null && marked.getAddress().equals(label.getAddress())) {
      marked.setBackground(Color.WHITE);
      current.setText("None");
      marked = null;
      return;
    }
    current.setText(label.getAddress());
    marked = label;
    marked.setBackground(Color.YELLOW);
    editor.setText(cells.getString(marked.getAddress()));
  }

  private void updateView() {
    for (SlotLabel s:labels) {
      s.setText(cells.getText(s.getAddress()));
    }
  }

  private void updateCells(String s) {
    if (marked == null) {
      status.setText("select a cell to edit");
      return;
    }
    try {
      cells.add(marked.getAddress(), s);
      updateView();
      status.setText("");
    } catch (XLException f) {
      status.setText(f.toString());
    }
  }

  public void clearAll() {
    cells.clear();
    updateView();
  }

  public void saveToFile(String filename) throws FileNotFoundException {
    cells.saveToFile(filename);
  }
  public void clearMarked() {
    cells.clearCell(marked.getAddress());
    updateView();
  }

  public void loadNewXL(Map <String, String> loadMap) {
    cells.clear();
    try {
      for(Entry<String, String> e: loadMap.entrySet()) {
        cells.add(e.getKey(), e.getValue());
      }
    } catch (XLException e2) {
      status.setText(e2.toString());
    }
    updateView();
    editor.setText(cells.getString(marked.getAddress()));
  }
}
