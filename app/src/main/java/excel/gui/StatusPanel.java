package excel.gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;

public class StatusPanel extends BorderPanel {

  CurrentLabel current;
  StatusLabel status;

  protected StatusPanel(StatusLabel statusLabel) {
    this.current = new CurrentLabel();
    this.status = statusLabel;
    add(WEST, current);
    add(CENTER, status);
  }

  public CurrentLabel getCurrentLabel() {
    return current;
  }
  public StatusLabel getStatusLabel() {
    return status;
  }
}
