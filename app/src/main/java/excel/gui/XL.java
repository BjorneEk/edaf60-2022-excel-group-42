package excel.gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;

import excel.model.CellMatrix;
import excel.gui.menu.Controller;
import javax.swing.JFrame;
import javax.swing.JPanel;
import excel.gui.menu.XLMenuBar;

public class XL extends JFrame {

  private static final int ROWS = 10, COLUMNS = 8;
  private XLCounter counter;
  private StatusLabel statusLabel = new StatusLabel();
  private XLList xlList;
  private Controller controller;
  private CellMatrix cells;

  public XL(XL oldXL) {
    this(oldXL.xlList, oldXL.counter);
  }

  public XL(XLList xlList, XLCounter counter) {
    super("Untitled-" + counter);
    this.xlList = xlList;
    this.counter = counter;
    cells = new CellMatrix();

    xlList.add(this);
    counter.increment();
    StatusPanel statusPanel = new StatusPanel(statusLabel);
    SheetPanel sheetPanel  = new SheetPanel(ROWS, COLUMNS);
    Editor editor      = new Editor();
    controller         = new Controller(cells, statusPanel, editor, sheetPanel.getSlotLabels());
    add(NORTH, statusPanel);
    add(CENTER, editor);
    add(SOUTH, sheetPanel);
    setJMenuBar(new XLMenuBar(this, xlList, statusLabel, controller));
    pack();
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setResizable(false);
    setVisible(true);
  }

  public void rename(String title) {
    setTitle(title);
    xlList.setChanged();
  }

  public static void main(String[] args) {
    new XL(new XLList(), new XLCounter());
  }
}
