package excel.gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;
import java.util.List;

public class SheetPanel extends BorderPanel {

  private SlotLabels sLabels;

  public SheetPanel(int rows, int columns) {
    add(WEST, new RowLabels(rows));
    sLabels = new SlotLabels(rows, columns);
    add(CENTER, sLabels);
  }

  public List<SlotLabel> getSlotLabels() {
    return sLabels.getSlotLabels();
  }
}
