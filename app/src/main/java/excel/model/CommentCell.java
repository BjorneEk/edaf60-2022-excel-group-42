package excel.model;

import excel.expr.ExprParser;
import excel.expr.Environment;

public class CommentCell extends Cell {

  public CommentCell(String input) {
    super(input);
  }

  public double value(Environment e) {
    return 0;
  }

  public String text(Environment e) {
    return str.substring(1);
  }

  public String toString() {
    return super.str;
  }
}
