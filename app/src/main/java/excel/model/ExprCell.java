package excel.model;

import excel.expr.Expr;
import excel.expr.Environment;

public class ExprCell extends Cell {

  private Expr ex;

  public ExprCell(String input, Expr ex) {
    super(input);
    this.ex = ex;
  }

  public double value(Environment e) {
    return ex.value(e);
  }

  public String text(Environment e) {
    return String.valueOf(ex.value(e));
  }

  public String toString() {
    return super.str;
  }
}
