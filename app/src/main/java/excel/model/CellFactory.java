package excel.model;


import excel.expr.Environment;
import excel.expr.ExprParser;
import java.io.IOException;

public class CellFactory {

  private ExprParser parser;

  public CellFactory() {
    parser = new ExprParser();
  }

  public Cell build(String input) throws IOException {
    if(input.trim().isEmpty()) {
      return new CommentCell(input);
    }
    if (input.charAt(0) == '#') {
      return new CommentCell(input);
    }
    return new ExprCell(input, parser.build(input));
  }
}
