package excel.model;

import excel.expr.Environment;
import java.io.FileNotFoundException;
import excel.util.XLException;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Set;

public class CellMatrix implements Environment  {

  private CellFactory       factory;
  private Map<String, Cell> matrix;

  public CellMatrix() {
    matrix  = new HashMap<>();
    factory = new CellFactory();
  }

  public void add(String addr, String input) throws XLException {
    if (input.trim().isEmpty()) {
      matrix.remove(addr);
      return;
    }

    Cell oldCell, newCell;

    oldCell = matrix.get(addr);
    try {
      newCell = factory.build(input);
    } catch (XLException e) {
      newCell = oldCell;
      throw e;
    } catch (IOException e) {
      newCell = oldCell;
      System.out.println(e.getMessage());
    }
    matrix.put(addr, newCell);
  }


  public String getString(String addr) {
    if (matrix.get(addr) == null) {
      return "";
    }
    return matrix.get(addr).toString();
  }

  public String getText(String addr) {
    if (matrix.get(addr) == null) {
      return "";
    }
    return matrix.get(addr).text(this);
  }

  public double value(String addr) {
    if(matrix.get(addr) == null) {
      return 0;
    }
    Cell c = matrix.get(addr);
    return c.value(this);
  }

  public void clearCell(String addr) {
    matrix.remove(addr);
  }

  public void clear() {
    matrix.clear();
  }

  public void saveToFile(String filepath) throws FileNotFoundException {
    XLPrintStream xlPrintStream = new XLPrintStream(filepath);
    xlPrintStream.save(matrix.entrySet());
  }
}
