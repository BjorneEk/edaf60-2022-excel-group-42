package excel.model;

import excel.expr.Environment;

public abstract class Cell {

  protected String str;

  public Cell(String str) {
    this.str = str;
  }

  public abstract double value(Environment e);

  public abstract String text(Environment e);

}
